def fib(n)
  if n == 0
    0
  elsif n == 1
    1
  else
    fib(n-1)+fib(n-2)
  end
end

puts "podaj liczbe:"
a = gets.chomp.to_i
puts fib(a)
