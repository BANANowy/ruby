require "./person.rb"

class Database
  def initialize
    @db = []
    load
  end

  def read
    a=""
    @db.each_with_index do |person,i|
      a+="#{i+1}. #{person.pretty_print}"
    end
    return a
  end

  def add(person,persist)
    @db.push(person)
    save if persist
  end

  def search(value)
    a=""
    @db.each do |x|
      if (x.name==value || x.surn==value || x.age==value)
        a+=x.pretty_print
      end
    end
    return a
  end

  def save
    f = File.open("baza.csv", "w")
    @db.each do |x|
      f.puts "#{x.name},#{x.surn},#{x.age}"
    end
    f.close
  end

  def load
    return if !File.exist?("baza.csv")
    f = File.open("baza.csv", "r")
    f.each do |x|
      dane = x.gsub("\n","")
      dane = dane.split(",")
      p=Person.new(dane[0],dane[1],dane[2])
      add p, false
    end
    f.close
  end

  def delete(usun)
    @db.delete_at( usun - 1)
    save
  end



end
