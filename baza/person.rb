class Person
  attr_accessor :name, :surn, :age
  def initialize(name, surn, age)
    @name = name
    @surn = surn
    @age = age
  end

  def pretty_print
    return "Imie: #{@name}, Nazwisko: #{@surn}, Wiek: #{@age}\n"
  end
end
