def to_i(n)
  a=n.split('')
  a.reverse!
  p=1
  sum=0
  a.each do |x|
    x=x.ord-48
    sum+=(x*p)
    p*=10
  end
  return sum
end
