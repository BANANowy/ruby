require "./database.rb"

db = Database.new

loop do
puts " 1.Dodaj uzytkownika\n 2.Wyswietl uzytkownika\n 3.Szukaj uzytkownika \n 4.Usun uzytkownika\n 0.Wyjscie\n"
n = gets.chomp.to_i
case n
  when 1
    puts "Podaj imie"
    imie = gets.chomp
    puts "Podaj nazwisko"
    nazwisko = gets.chomp
    puts "Podaj wiek"
    wiek = gets.chomp
    p=Person.new(imie,nazwisko,wiek)
    db.add p,true
  when 2
    puts db.read
  when 3
    puts "Co chcesz znalezc?"
    szuk=gets.chomp
    puts db.search(szuk)
  when 4
    puts db.read
    puts "Co chcesz usunac? (numer)"
    usun=gets.chomp.to_i
    db.delete(usun)
  when 0
    break
  end
end
