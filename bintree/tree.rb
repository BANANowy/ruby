class Tree
  def initialize
    @tree = []
  end

  def search(element) # szuka wysokosci oraz indeksu danego elementu
    n = 0
    h = 0 # wysokosc
    loop do
      if @tree[n] == element
        puts "Dany element znajduje sie na wysokosci #{h} i ma indeks #{n}."
        break
      elsif @tree[n].nil?
        puts 'Dany element nie znajduje sie w drzewie.'
        break
      elsif @tree[n] > element
        n = (2 * n) + 1
      else # @tree[n] <= element
        n = (2 * n) + 2
      end
      h += 1
    end
  end

  def insert(element) # umieszcza elementy w drzewie
    n = 0 # indeks ojca
    loop do
      if !@tree[n]
        @tree[n] = element
        puts "#{element} umieszczono w elemencie tablicy drzewa o indeksie #{n}"
        break
      elsif @tree[n] > element
        n = (2 * n) + 1
      else # @tree[n] <= element
        n = (2 * n) + 2
      end
    end
  end

  def height # zwraca wysokosc drzewa
    length = @tree.length
    n = 1
    h = 1 # wysokosc
    loop do
      if length.zero?
        return length
      elsif length <= n
        return h
      else
        n = (n * 2) + 1
      end
      h += 1
    end
  end

  def size # zwraca liczbe elementow w drzewie
    length = @tree.length
    n = 0
    @tree.each do |x|
      n += 1 unless x
    end
    length - n
  end

  def remove
    # do zrobienia
  end

  def print_tree
    n = 0
    if @tree.empty?
      puts 'Drzewo jest puste.'
    else
      puts "\n"
      height.times do
        puts @tree[n..(2 * n)].join(' ')
        n = (2 * n) + 1
      end
      puts "\n"
    end
  end
end
