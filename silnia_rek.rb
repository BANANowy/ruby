def silnia(n)
  if n == 0
    1
  else
    n * silnia(n-1)
  end
end

puts "wprowadz liczbe"
a = gets.chomp.to_i
puts "#{a}! = #{silnia(a)}"
