class Pet
    def initialize(name)
      @name = name
    end

    def eat
      puts "#{@name} je karme."
    end

  end

  class Dog < Pet
    def speak
      puts "#{@name} zrobil hau."
    end
  end

class Cat < Pet
  def speak
    puts "#{@name} zrobil miau."
  end
end

c1 = Cat.new("Judi")
c2 = Cat.new("Nocka")
d1 = Dog.new("Ruby")
d2 = Dog.new("Kabi")

p1 = Pet.new("UndefinedPet")

p1.speak

c1.eat
c2.eat
d1.eat
d2.eat

c1.speak
c2.speak
d1.speak
d2.speak
