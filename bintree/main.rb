require './tree.rb'

drzewo = Tree.new

loop do
  puts "1. Dodaj element do drzewa\n2. Wysokosc drzewa
3. Ilosc elementow w drzewie\n4. Wyswietl drzewo
5. Szukaj elementu w drzewie\n0. Wyjscie\n"
  n = gets.chomp.to_i
  case n
  when 1
    puts 'Wpisz element, ktory chcesz dodac do drzewa:'
    element = gets.chomp.to_i
    drzewo.insert(element)
  when 2
    puts "Wysokosc drzewa: #{drzewo.height}"
  when 3
    puts 'W drzewie znajduje sie #{drzewo.size} elementow.'
  when 4
    drzewo.print_tree
  when 5
    puts 'Wpisz element, ktory chcesz znalezc.'
    element = gets.chomp.to_i
    drzewo.search(element)
  when 0
    break
  end
end
