def quicksort(a)
    if a.length <= 1
      return a
    else
      l = []
      g = []
      pivot = a.pop
      a.each do |x|
        if x > pivot
          g.push(x)
        else
          l.push(x)
        end
      end
      return quicksort(l) + [pivot] + quicksort(g)
    end
end

tablica = [7,2,4,8,3,5]
puts "[#{quicksort(tablica).join(",")}]"
